import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './src/components/HomeScreen';
import LoginScreen from './src/components/LoginScreen';
import ClienteScreen from './src/components/ClienteScreen';
import AdministradorScreen from './src/components/AdministradorScreen';


const Stack = createStackNavigator();

function AppNavigator() {
  return (
    <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen name="Login" component={LoginScreen} />
      <Stack.Screen name="Cliente" component={ClienteScreen} />
      <Stack.Screen name="Administrador" component={AdministradorScreen} />
    </Stack.Navigator>
    </NavigationContainer>
  );
}

export default AppNavigator;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
