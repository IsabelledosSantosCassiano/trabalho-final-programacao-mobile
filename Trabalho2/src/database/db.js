import * as SQLite from 'expo-sqlite';
import axios from 'axios';

const db = SQLite.openDatabase('db.db');

// Função de criação das tabelas Clientes, Administradores e paises caso nao estejam criadas
const createTables = () => {
  db.transaction((tx) => {
    tx.executeSql(
      'CREATE TABLE IF NOT EXISTS clientes (id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT, senha TEXT);'
    );
    tx.executeSql(
      'CREATE TABLE IF NOT EXISTS administradores (id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT, senha TEXT);'
    );
    tx.executeSql(
      'CREATE TABLE IF NOT EXISTS paises (id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT, capital TEXT, regiao TEXT, lingua TEXT);'
    );
  });
};

//chama a função que cria as tabelas do banco de dados
createTables();

// faz a autenticação do usuário pegando seu nome, senha e tipo
export const login = (nome, senha, userType) => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      //caso o tipo do usuário seja cliente seu nome é passado para a variavel tablename como a tabela clientes
      //caso contrário é passado como administradores, isso é feito para o reconhecimento do nome da tabela sem precisar duplicar código
      const tableName = userType === 'cliente' ? 'clientes' : 'administradores';

      tx.executeSql(
        `SELECT * FROM ${tableName} WHERE nome = ? AND senha = ?`,
        [nome, senha],
        (_, { rows }) => {
          if (rows.length > 0) {  //caso exista o usuário no banco o mesmo é dado como estado resolve e passa para a  proxima tela
            resolve();
          } else {  
            //caso nao exista no banco o usuário é inserido e tem acesso a próxima tela
            tx.executeSql(
              `INSERT INTO ${tableName} (nome, senha) VALUES (?, ?)`,
              [nome, senha],
              (_, { rowsAffected }) => {
                if (rowsAffected > 0) {
                  resolve();
                } else {
                  reject(new Error('Falha ao inserir usuário.'));
                }
              },
              (_, error) => {
                reject(error);
              }
            );
          }
        },
        (_, error) => {
          reject(error);
        }
      );
    });
  });
};


//funções que foi tentado implementá-las mas o banco não apresentava a lista dos países na tela após a sua inserção


//função que puxaria os dados da API Countries e inseriria na tabela paises no banco de dados
export const buscaPaises = () => {
  return new Promise((resolve, reject) => {
    axios
      .get('https://restcountries.com/v2/all')
      .then((response) => {
        const countries = response.data;
        const values = countries.map((country) => [
          country.name,
          country.capital,
          country.region,
          country.languages[0].name,
        ]);

        db.transaction((tx) => {
          const placeholders = values.map(() => '(?, ?, ?, ?)').join(', ');
          const flattenedValues = values.flat();

          //insere os países no da Api no banco de dados
          tx.executeSql(
            `INSERT INTO paises (nome, capital, regiao, lingua) VALUES ${placeholders}`,
            [...flattenedValues],
            (_, { rowsAffected }) => {
              if (rowsAffected === countries.length) {
                console.log('Países inseridos com sucesso.');
                resolve();
              } else {
                reject(
                  new Error(
                    'Falha ao inserir países no banco de dados. Número de linhas afetadas não corresponde.'
                  )
                );
              }
            },
            (_, error) => {
              reject(
                new Error(`Erro ao inserir países no banco de dados: ${error.message}`)
              );
            }
          );
        });
      })
      .catch((error) => {
        reject(new Error(`Erro ao obter dados dos países: ${error.message}`));
      });
  });
};

//função que puxaria os países inseridos no banco de dados
export const mostrarPaises = () => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        'SELECT * FROM paises',
        [],
        (_, { rows }) => {
          const countries = rows._array;
          resolve(countries);
        },
        (_, error) => {
          reject(
            new Error(`Erro ao buscar países do banco de dados: ${error.message}`)
          );
        }
      );
    });
  });
};

//função que atualizaria os campos de um determinado país no banco de dados
export const atualizarPaises = (country) => {
  return new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        'UPDATE paises SET nome=?, capital=?, regiao=?, lingua=? WHERE id=?',
        [country.nome, country.capital, country.regiao, country.lingua, country.id],
        (_, { rowsAffected }) => {
          if (rowsAffected > 0) {
            console.log('País atualizado no banco de dados:', country.nome);
            resolve();
          } else {
            reject(new Error('Nenhum país atualizado.'));
          }
        },
        (_, error) => {
          console.error('Erro ao atualizar país no banco de dados:', error);
          reject(error);
        }
      );
    });
  });
};

