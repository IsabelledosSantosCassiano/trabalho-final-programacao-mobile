import { StyleSheet } from 'react-native';
export const styles = StyleSheet.create({
  
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },

  button: {
    backgroundColor: '#4040ff',
    borderRadius: 10,
    padding: 10,
    marginBottom: 10,
  },

  saveOrder: {
    backgroundColor: '#4040ff',
    borderRadius: 10,
    padding: 10,
    marginBottom: 10,
    width: 190,
    
  },

  clienteButton: {
    borderRadius: 10,
    padding: 10,
    paddingHorizontal: 30,
    marginBottom: 10,
    backgroundColor: '#5e9efd',
  },
  buttonText: {
    color: '#fff',
    fontSize: 18,
    textAlign: 'center',
  },
  input: {
    width: '80%',
    height: 40,
    borderColor: '#4040ff',
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 10,
    paddingLeft: 10,
  },

  //estilo pra espaçar os elementos
  espacamento: {
    height: 10, 
  },

  //estilo para deletar um país
  deleteIcon:{
    color: '#f00',
    fontWeight: 'bold',
  },


});