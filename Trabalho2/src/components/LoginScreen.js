import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { login } from '../database/db';
import { styles } from '../styles/appStyles';

function LoginScreen() {
  const navigation = useNavigation();
  const route = useRoute();  // informa a rota atual
  const [nome, setNome] = useState('');
  const [senha, setSenha] = useState('');


  //verificação de senha 
  const handleLogin = () => { 
    if (nome.trim() === '' || senha.trim() === '') {   //retira os espaços para verificar preenchimento
      alert('Por favor, preencha todos os campos.');
      return;
    }

  //verificação do login
    login(nome, senha, route.params.userType)
    .then(() => {
      console.log('Usuário inserido no banco de dados.');  //mostra no console.log se houve sucesso na inserção do usuário
      if (route.params.userType === 'cliente') {
        navigation.navigate('Cliente'); 
      } else if (route.params.userType === 'administrador') {
        navigation.navigate('Administrador');
      } else {
        alert('Ocorreu um erro durante o login. Por favor, verifique suas credenciais e tente novamente.');
      }
    })
    .catch((error) => {
      alert('Falha ao fazer login: ' + error.message);
    });
};

//mostra a parte visual da tela de login de acordo com o botão clicado pelo usuário (cliente ou administrador)
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Login {route.params.userType}</Text>
      <TextInput
        style={styles.input}
        placeholder="Insira seu nome"
        value={nome}
        onChangeText={setNome}
      />
      <TextInput
        style={styles.input}
        placeholder="Insira sua senha"
        secureTextEntry
        value={senha}
        onChangeText={setSenha}
      />
      <TouchableOpacity style={styles.button} onPress={handleLogin}>
        <Text style={styles.buttonText}>Logar</Text>
      </TouchableOpacity>
    </View>
  );
}

export default LoginScreen;
