import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { styles } from '../styles/appStyles';

function HomeScreen() {
  const navigation = useNavigation(); //permite a navegação entre as telas

  //função chamada quando o usuário clica no botão cliente redirecionando o mesmo para a tela de login do cliente
  const clientePress = () => {
    navigation.navigate('Login', { userType: 'cliente' }); //recebe o tipo cliente
  };
  //função chamada quando o usuário clica no botão administrador redirecionando o mesmo para a tela de login do administrador
  const administradorPress = () => {
    navigation.navigate('Login', { userType: 'administrador' });//recebe o tipo administrador
  };

  //mostra os botões de escolha da tela inicial
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Bem-vindo(a)</Text>
      <TouchableOpacity style={[styles.button, styles.clienteButton]} onPress={clientePress}>
        <Text style={styles.buttonText}>Cliente</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={administradorPress}>
        <Text style={styles.buttonText}>Administrador</Text>
      </TouchableOpacity>
    </View>
  );
}

export default HomeScreen;
