import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, TouchableOpacity, TextInput, Button } from 'react-native';
import axios from 'axios';
import { styles } from '../styles/appStyles';

function AdministradorScreen() {
  const [countries, setCountries] = useState([]);
  const [loading, setLoading] = useState(true);
  const [newCountryName, setNewCountryName] = useState('');
  const [newCountryCapital, setNewCountryCapital] = useState('');
  const [newCountryRegion, setNewCountryRegion] = useState('');
  const [newCountryLanguage, setNewCountryLanguage] = useState('');

  useEffect(() => {
    infoAPICountries();
  }, []);

  //Função que pega os dados da API Rest Countries por requisição HTTP tratando erros
  const infoAPICountries = async () => {
    try {
      const response = await axios.get('https://restcountries.com/v2/all');
      setCountries(response.data);
      setLoading(false);
    } catch (error) {
      console.error('Ocorreu um erro durante a obtenção dos dados:', error);
      alert('Ocorreu um erro durante a obtenção dos dados. Por favor, tente novamente.');
    }
  };

  // Função que adiciona um novo país na lista
  const adicionarPais = () => {
    const newCountry = {
      name: newCountryName,
      capital: newCountryCapital,
      region: newCountryRegion,
      languages: [{ name: newCountryLanguage }],
    };

    // É utilizado o método sort que faz a comparação do nome do país inserido com os da lista
    // até que encontre a posição adequada do nome em ordem alfabética
    const updatedCountries = [...countries, newCountry].sort((a, b) => a.name.localeCompare(b.name));

    setCountries(updatedCountries); // Atualiza a lista de países

// Após isso o nome, capital, regiao e lingua do país sao setados na ordem correta na lista
    setNewCountryName(''); 
    setNewCountryCapital('');
    setNewCountryRegion('');
    setNewCountryLanguage('');
  };

  //função que deleta o país selecionado da lista de países
  //retornando um array com todos os elementos originais que nao foram deletados
  const deletarPais = (country) => {
    const updatedCountries = countries.filter((c) => c.name !== country.name);
    setCountries(updatedCountries);
  };

  //organiza a lista dos países na tela
  const itemPais = ({ item }) => (
    <View>
      <Text>
        <strong>País: </strong>
        {item.name}
      </Text>
      <Text>
        <strong>Capital: </strong>
        {item.capital}
      </Text>
      <Text>
        <strong>Região: </strong>
        {item.region}
      </Text>
      <Text>
        <strong>Língua: </strong>
        {item.languages && item.languages.length > 0 ? item.languages[0].name : 'N/A'}
      </Text>

      <TouchableOpacity onPress={() => deletarPais(item)}>
        <Text style={styles.deleteIcon}>X</Text>
      </TouchableOpacity>

      <View style={styles.espacamento} />
    </View>
  );

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Criar novo País</Text>
      <TextInput
        style={styles.input}
        placeholder="Nome do país"
        value={newCountryName}
        onChangeText={setNewCountryName}
      />

      <TextInput
        style={styles.input}
        placeholder="Nome da capital"
        value={newCountryCapital}
        onChangeText={setNewCountryCapital}
      />

      <TextInput
        style={styles.input}
        placeholder="Nome da região"
        value={newCountryRegion}
        onChangeText={setNewCountryRegion}
      />

      <TextInput
        style={styles.input}
        placeholder="Nome da língua"
        value={newCountryLanguage}
        onChangeText={setNewCountryLanguage}
      />

      <Button title="Adicionar País" onPress={adicionarPais} />

      <Text style={styles.title}>Lista de Países</Text>

      {loading ? (
        <Text>Carregando...</Text>
      ) : (
        <FlatList
          data={countries}
          renderItem={itemPais}
          keyExtractor={(item, index) => index.toString()}
        />
      )}
    </View>
  );
}

export default AdministradorScreen;