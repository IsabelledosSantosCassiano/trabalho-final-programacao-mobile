import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, TouchableOpacity, TextInput } from 'react-native';
import axios from 'axios';
import { styles } from '../styles/appStyles';

function ClienteScreen() {
  const [countries, setCountries] = useState([]); //lista dos países
  const [loading, setLoading] = useState(true); //avisa o carregamento dos dados
  const [error, setError] = useState(null); //puxa a informação de um possível erro
  const [editCountry, setEditCountry] = useState(null); //pais que está sendo editado
  const [editedCountries, setEditedCountries] = useState([]); //lista dos países editados
  

  //Função que carrega os dados dos países assim que o componente é montado
  useEffect(() => {
    infoAPICountries(); 
  }, []);


  //Função que pega os dados da API Rest Countries por requisição HTTP tratando erros
  const infoAPICountries = async () => {
    try {
      const response = await axios.get('https://restcountries.com/v2/all'); 
      setCountries(response.data); //atualiza a lista de países
      setLoading(false); //indica que a lista foi carregada 

    } catch (error) {
      console.error('Ocorreu um erro durante a obtenção dos dados:', error);
      alert('Ocorreu um erro durante a obtenção dos dados. Por favor, tente novamente.');
      setLoading(false);
      setError(error);
    }
  };

  // Função atualiza a lista de países editados, substituindo o nome do país pelo valor inserido
  //mapeia o array editedCountries e cria um novo array atualizado
  
  const atualizaNomePais = (text, country) => {
    const updatedCountries = editedCountries.map((c) => {
      if (c.alpha2Code === country.alpha2Code) {    //faz a verificação do código do país
        return { ...c, name: text };     //se igual retorna o dado atualizado
      }
      return c;   //se for diferente retorna o dado sem alteração
    });
    //a lista de países atualizados é setada com o novo array atualizado
    setEditedCountries(updatedCountries);
  };

  //atualiza a lista de países editados, substituindo o nome da capital pelo valor inserido
  const atualizaNomeCapital = (text, country) => {
    const updatedCountries = editedCountries.map((c) => {
      if (c.alpha2Code === country.alpha2Code) {
        return { ...c, capital: text };
      }
      return c;
    });
    setEditedCountries(updatedCountries);
  };

  //atualiza a lista de países editados, substituindo o nome da regiao pelo valor inserido
  const atualizaNomeRegiao = (text, country) => {
    const updatedCountries = editedCountries.map((c) => {
      if (c.alpha2Code === country.alpha2Code) {
        return { ...c, region: text };
      }
      return c;
    });
    setEditedCountries(updatedCountries);
  };

 //atualiza a lista de países editados, substituindo o nome da lingua pelo valor inserido
  const atualizaNomeLingua = (text, country) => {
    const updatedCountries = editedCountries.map((c) => {
      if (c.alpha2Code === country.alpha2Code) {
        return { ...c, languages: [{ name: text }] };
      }
      return c;
    });
    setEditedCountries(updatedCountries);
  };

  //função chamada quando o usuário clica para iniciar a edição dos campos 
  const handleCountryEdit = (country) => {
    setEditCountry(null);
    const updatedCountries = countries.map((c) => {
      const editedCountry = editedCountries.find((edited) => edited.alpha2Code === c.alpha2Code);
      if (editedCountry) {
        return editedCountry;
      }
      return c;
    });

    //Função que faz a ordenação do país salvo em ordem alfabética usando a função .sort
    const sortedCountries = [...updatedCountries].sort((a, b) => a.name.localeCompare(b.name));

    setCountries(sortedCountries);
    setEditedCountries([]);
  };

  const startCountryEdit = (country) => {
    setEditCountry(country);

  // Verifica se existe algum país no array editedCountries com o mesmo código do país fornecido
    const editedCountry = editedCountries.find((edited) => edited.alpha2Code === country.alpha2Code);
    if (editedCountry) {
      setEditedCountries([editedCountry]);
    } else {
      setEditedCountries([...editedCountries, country]);
    }
  };

  //Função que recebe um objeto com com uma propiedade item
  //renderiza um país na tela do usuário
  const renderCountry = ({ item }) => {
    const isEditing = editCountry === item;
    const editedCountry = editedCountries.find((edited) => edited.alpha2Code === item.alpha2Code);

//Parte visual da tela 
//obs. na linguagem há um array de 3 opções por isso foi setada a primeira posição em item.languages[0].name
    return (
      <>
        <TouchableOpacity style={styles.countryContainer} onPress={() => startCountryEdit(item)}>
          {isEditing ? (
            <>
            
              <Text style={styles.editLabel}><strong>Editar País:</strong></Text>
              <TextInput
                style={styles.input}
                value={editedCountry?.name || item.name}
                onChangeText={(text) => atualizaNomePais(text, item)}
              />
              <Text style={styles.editLabel}><strong>Editar Capital:</strong></Text>
              <TextInput
                style={styles.input}
                value={editedCountry?.capital || item.capital}
                onChangeText={(text) => atualizaNomeCapital(text, item)}
              />
              <Text style={styles.editLabel}><strong>Editar Região:</strong></Text>
              <TextInput
                style={styles.input}
                value={editedCountry?.region || item.region}
                onChangeText={(text) => atualizaNomeRegiao(text, item)}
              />
              <Text style={styles.editLabel}><strong>Editar Língua:</strong></Text>
              <TextInput
                style={styles.input}
                value={editedCountry?.languages[0].name || item.languages[0].name}
                onChangeText={(text) => atualizaNomeLingua(text, item)}
              />

              <TouchableOpacity style={styles.saveOrder} onPress={() => handleCountryEdit(item)}>
                <Text style={styles.buttonText}>Salvar e Ordenar</Text>
              </TouchableOpacity>
            </>
          ) : (
            <>
              <Text style={styles.countryText}><strong>País: </strong>{item.name}</Text>
              <Text><strong>Capital: </strong>{item.capital}</Text>
              <Text><strong>Região: </strong> {item.region}</Text>
              <Text><strong>Lingua: </strong> {item.languages[0].name}</Text>
              <View style={styles.espacamento} /> 
            </>
          )}
        </TouchableOpacity>
        <View style={styles.separator} />
      </>

    );
  };

//caso os dados estejam sendo carregados mostra na tela 
  if (loading) {
    return (
      <View style={styles.container}>
        <Text>Carregando...</Text>
      </View>
    );
  }
// verifica se ocorreu um erro ao obter os dados
  if (error) {
    return (
      <View style={styles.container}>
        <Text>Ocorreu um erro: {error}</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Lista de Países</Text>
      <FlatList
        data={countries}
        keyExtractor={(item) => item.alpha2Code}
        renderItem={renderCountry}
        contentContainerStyle={styles.countryList}
      />
    </View>
  );
}

export default ClienteScreen;